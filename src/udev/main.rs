// SPDX-License-Identifier: GPL-3.0-only
/*
 * The "guichet-udev".
 *
 * (C) Copyright 2019-2022 Stephane Neveu
 *
 * This file is the main file for udev.
 */

//#![feature(toowned_clone_into)]
#![feature(atomic_from_mut)]
//#![feature(dir_entry_ext2)]

extern crate libc;
extern crate regex;
extern crate udev;

use anyhow::anyhow;
use clap::{crate_version, Arg, Command};
use regex::Regex;
use udev::Event;
//use std::fmt::format;
use std::fs::{self, create_dir_all};
//use std::os::unix::fs::DirEntryExt2;
use std::path::PathBuf;
use std::thread as sthread;
use std::{ffi::OsStr, net::TcpListener, thread::spawn};
use tungstenite::{
    accept_hdr,
    handshake::server::{Request, Response},
    Message,
};
extern crate minisign;
extern crate proc_mounts;
extern crate serde;
extern crate serde_json;
extern crate sys_mount;

#[macro_use]
extern crate serde_derive;

use crossbeam_utils::thread;
use libc::{c_int, c_short, c_ulong, c_void};
use minisign::PublicKeyBox;
use minisign::SignatureBox;
use nom::bytes::complete::take;
use nom::error::Error;
use nom::number::complete::be_u32;
use proc_mounts::MountIter;
use std::fmt::Write;
use std::fs::File;
use std::io::Cursor;
use std::io::Read;
use std::io::Seek;
use std::io::SeekFrom;
use std::os::unix::io::AsRawFd;
use std::path::Path;
use std::ptr;
use std::str;
use std::sync::Arc;
use std::time::Duration;
use sys_mount::unmount;
use sys_mount::{Mount, MountFlags, SupportedFilesystems, Unmount, UnmountFlags};

mod errors;
use crate::errors::*;
//use std::process::exit;

#[repr(C)]
struct pollfd {
    fd: c_int,
    events: c_short,
    revents: c_short,
}

#[repr(C)]
struct sigset_t {
    __private: c_void,
}

#[allow(non_camel_case_types)]
type nfds_t = c_ulong;

const POLLIN: c_short = 0x0001;

extern "C" {
    fn ppoll(
        fds: *mut pollfd,
        nfds: nfds_t,
        timeout_ts: *mut libc::timespec,
        sigmask: *const sigset_t,
    ) -> c_int;
}
#[derive(Serialize, Deserialize, Debug)]
pub struct Usbkeys {
    usb_in: Vec<String>,
    usb_out: Vec<String>,
    usb_undef: Vec<String>,
}

fn get_signature(device: &str) -> Result<String> {
    let offset = 512;
    let mut f = File::options()
        .read(true)
        .open(device)
        .context("Cannot open device.")?;
    let mut buf = vec![0u8; 2048];
    f.seek(SeekFrom::Start(offset))?;
    f.read_exact(&mut buf)?;

    let (i, len) = be_u32::<&[u8], nom::error::Error<&[u8]>>(&buf).map_err(|err| {
        err.map(|err| Error::new(String::from_utf8(err.input.to_vec()), err.code))
    })?;
    let (_, signature) = take::<u32, &[u8], nom::error::Error<&[u8]>>(len)(i).map_err(|err| {
        err.map(|err| Error::new(String::from_utf8(err.input.to_vec()), err.code))
    })?;
    let signature = str::from_utf8(signature)?;
    //    match parser(&bufstr) {
    //        Ok(signature) => {
    //            println!("{}",signature);
    //            signature},
    //        Err(err) => err.to_string(),
    //    };
    Ok(signature.to_string())
}

fn is_signed(
    device: &str,
    pubkey_path: &str,
    id_vendor_id: &str,
    id_model_id: &str,
    id_revision: &str,
    id_serial: &str,
) -> Result<bool> {
    match get_signature(device) {
        Ok(signature) => {
            //println!("Read signature from key: {:?}", signature);
            let pubkey_path = pubkey_path;

            let pk_box_str = fs::read_to_string(pubkey_path)?;
            let signature_box = SignatureBox::from_string(&signature)?;
            // Load the public key from the string.
            let pk_box = PublicKeyBox::from_string(&pk_box_str)?;
            let pk = pk_box.into_public_key()?;
            // And verify the data.
            let data = format!(
                "{}/{}/{}/{}/{}",
                id_vendor_id, id_model_id, id_revision, id_serial, "out"
            );
            println!("{}", data);
            let data_reader = Cursor::new(&data);
            let verified = minisign::verify(&pk, &signature_box, data_reader, true, false, false);
            println!("USB device is signed: {:?}", verified);
            match verified {
                Ok(()) => Ok(true),
                Err(_) => Ok(false),
            }
        }
        Err(_) => Ok(false),
    }
}

fn copy_device_in(device: &Path) -> Result<()> {
    let dir = tempfile::tempdir()?;
    //println!("Tmp dir is: {:?}", dir);
    let mount_point = dir.path();
    println!(
        "Unsigned USB device {:?} will be mounted on path: {:?}",
        device, mount_point
    );
    let supported = SupportedFilesystems::new()?;
    let mount_result = Mount::new(
        device,
        mount_point,
        &supported,
        MountFlags::RDONLY | MountFlags::NOSUID | MountFlags::NOEXEC | MountFlags::NODEV,
        None,
    );
    match mount_result {
        Ok(mount) => {
            // Copying file to the mounted device.
            println!("Unsigned device is mounted on: {:?}", mount_point);
            copy_files_in(&mount_point.to_path_buf())?;
            // Make the mount temporary, so that it will be unmounted on drop.
            let _mount = mount.into_unmount_drop(UnmountFlags::DETACH);
        }
        Err(why) => {
            eprintln!("Failed to mount unsigned device: {}", why);
            let reg = Regex::new(r"/tmp/\.tmp.*")?;
            for mount in MountIter::new()? {
                let mnt = mount.as_ref().unwrap().dest.to_str().unwrap();
                if reg.is_match(mnt) {
                    println!("Will umount: {}", mnt);
                }
            }
            //exit(1);
        }
    }
    Ok(())
}

fn move_device_out(device: &Path) -> Result<PathBuf> {
    let dir = tempfile::tempdir()?;
    //println!("Tmp dir is: {:?}", dir);
    let mount_point = dir.path();
    println!(
        "Signed USB device {:?} will be mounted on path: {:?}",
        device, mount_point
    );
    let supported = SupportedFilesystems::new()?;
    let mount_result = Mount::new(
        device,
        mount_point,
        &supported,
        MountFlags::NOEXEC | MountFlags::NOSUID | MountFlags::NODEV,
        None,
    );
    match mount_result {
        Ok(mount) => {
            // Moving files to the mounted device.
            println!("Temporary out mount point: {:?}", mount_point);
            move_files_out(&mount_point.to_path_buf())?;
            // Make the mount temporary, so that it will be unmounted on drop.
            let _mount = mount.into_unmount_drop(UnmountFlags::DETACH);
        }
        Err(why) => {
            eprintln!("Failed to mount device: {}", why);
            //exit(1);
        }
    }
    Ok(mount_point.to_path_buf())
}

fn copy_files_in(mount_point: &PathBuf) -> Result<()> {
    let mut dir = fs::read_dir(&mount_point)?;
    thread::scope(|s| {
        //while let Some(entry) = dir.next() {
        for entry in dir.by_ref() {
            s.spawn(move |_| {
                //let entry = entry.unwrap().file_name();
                match entry {
                    Ok(entry) => {
                        println!("New entry file found: {:?}.", &entry);
                        let entry = entry.file_name();
                        let entry_str = entry.to_string_lossy();
                        //Replacing any ? in case conversion failed
                        let path_to_read =
                            format!("{}{}{}", &mount_point.to_string_lossy(), "/", &entry_str);

                        let entry_cleaned = str::replace(&entry_str, "?", "-");

                        let path_to_write = format!(
                            "{}{}",
                            "/var/local/in/",
                            diacritics::remove_diacritics(&entry_cleaned)
                        );
                        match fs::metadata(&path_to_read) {
                            Ok(mtdata) => {
                                if Path::new(&path_to_read).exists() && !mtdata.is_dir() {
                                    match fs::copy(&path_to_read, &path_to_write) {
                                        Ok(_) => println!(
                                            "File {} copied to {}.",
                                            path_to_read, path_to_write
                                        ),
                                        Err(e) => {
                                            println!(
                                                "Error while copying file {}: {:?}",
                                                path_to_read, e
                                            );
                                            let mut report =
                                                format!("{}{}", path_to_write, ".failed");
                                            match File::create(&report) {
                                                Ok(_) => println!("io-error report file created."),
                                                Err(why) => {
                                                    eprintln!(
                                                        "Failed to create io-error report {:?}: {}",
                                                        report, why
                                                    );
                                                }
                                            }
                                            match writeln!(
                                                report,
                                                "Error while copying file: {:?}",
                                                e
                                            ) {
                                                Ok(_) => println!("io-error report file created."),
                                                Err(why) => {
                                                    eprintln!(
                                                    "Failed to write into io-error report {:?}: {}",
                                                    report, why
                                                );
                                                }
                                            }
                                            match unmount(mount_point, UnmountFlags::DETACH) {
                                                Ok(()) => {
                                                    println!(
                                                        "Early removing mount point: {:?}",
                                                        mount_point
                                                    )
                                                }
                                                Err(why) => {
                                                    eprintln!(
                                                        "Failed to unmount {:?}: {}",
                                                        mount_point, why
                                                    );
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            Err(why) => eprintln!(
                                "Thread error: Cannot get metadata for file {:?}: {:?}. Terminating thread...",
                                path_to_read, why
                            ),
                        };
                    }
                    Err(_) => {
                        eprintln!("Thread error: Cannot read file name entry. Terminating thread...")
                    }
                }
            });
        }
    })
    .expect("Cannot scope threads !");
    println!("Incoming files copied.");
    Ok(())
}

fn move_files_out(mount_point: &PathBuf) -> Result<()> {
    let dir = fs::read_dir("/var/local/out/")?;
    //while let Some(entry) = dir.next() {
    for entry in dir {
        let entry = entry?;
        println!("New entry found: {:?}.", entry.file_name());

        let path_to_write = format!(
            "{}{}{}",
            &mount_point.to_string_lossy(),
            "/",
            diacritics::remove_diacritics(&entry.file_name().to_string_lossy())
        );
        let path_to_read = format!(
            "{}{}",
            "/var/local/out/",
            entry.file_name().to_string_lossy().into_owned()
        );
        if !fs::metadata(&path_to_read)?.is_dir() {
            match fs::copy(&path_to_read, &path_to_write) {
                Ok(_) => println!("Copying file: {} to signed device.", path_to_read),
                Err(e) => {
                    println!(
                        "Error while copying file to signed device {}: {:?}",
                        path_to_read, e
                    );
                    match unmount(mount_point, UnmountFlags::DETACH) {
                        Ok(()) => println!("Early removing mount point: {:?}", mount_point),
                        Err(why) => {
                            eprintln!("Failed to unmount {:?}: {}", mount_point, why);
                        }
                    }
                }
            }
            fs::remove_file(&path_to_read)?;
            println!("Removing file: {}.", path_to_read);
        }
    }
    println!("Moving files to out device done.");
    Ok(())
}
fn busy_in() -> Result<(), anyhow::Error> {
    if !Path::new("/var/lock/leguichet").exists() {
        create_dir_all("/var/lock/leguichet")?;
    } else if Path::new("/var/lock/leguichet/leguichet-out").exists() {
        fs::remove_file("/var/lock/leguichet/leguichet-out")?;
    } else if Path::new("/var/lock/leguichet/leguichet-transit").exists() {
        fs::remove_file("/var/lock/leguichet/leguichet-transit")?;
    } else if !Path::new("/var/lock/leguichet/leguichet-in").exists() {
        File::create("/var/lock/leguichet/leguichet-in")?;
    } else {
    }
    Ok(())
}

fn busy_out() -> Result<(), anyhow::Error> {
    if !Path::new("/var/lock/leguichet").exists() {
        create_dir_all("/var/lock/leguichet")?;
    } else if Path::new("/var/lock/leguichet/leguichet-in").exists() {
        fs::remove_file("/var/lock/leguichet/leguichet-in")?;
    } else if Path::new("/var/lock/leguichet/leguichet-transit").exists() {
        fs::remove_file("/var/lock/leguichet/leguichet-transit")?;
    } else if !Path::new("/var/lock/leguichet/leguichet-out").exists() {
        File::create("/var/lock/leguichet/leguichet-out")?;
    } else {
    }
    Ok(())
}

fn ready_in() -> Result<(), anyhow::Error> {
    if Path::new("/var/lock/leguichet/leguichet-in").exists() {
        fs::remove_file("/var/lock/leguichet/leguichet-in")?;
    }
    Ok(())
}

fn ready_out() -> Result<(), anyhow::Error> {
    if Path::new("/var/lock/leguichet/leguichet-out").exists() {
        fs::remove_file("/var/lock/leguichet/leguichet-out")?;
    }
    Ok(())
}

fn get_attr_udev(event: Event) -> Result<String, anyhow::Error> {
    let id_vendor_id = event
        .property_value(
            OsStr::new("ID_VENDOR_ID")
                .to_str()
                .ok_or_else(|| anyhow!("Cannot convert ID_VENDOR_ID to str."))?,
        )
        .ok_or_else(|| anyhow!("Cannot get ID_VENDOR_ID from event."))?;
    let id_model_id = event
        .property_value(
            OsStr::new("ID_MODEL_ID")
                .to_str()
                .ok_or_else(|| anyhow!("Cannot convert ID_MODEL_ID to str."))?,
        )
        .ok_or_else(|| anyhow!("Cannot get ID_MODEL_ID from event."))?;
    let id_revision = event
        .property_value(
            OsStr::new("ID_REVISION")
                .to_str()
                .ok_or_else(|| anyhow!("Cannot convert ID_REVISION to str."))?,
        )
        .ok_or_else(|| anyhow!("Cannot get ID_REVISION from event."))?;

    let product = format!(
        "{}/{}/{}",
        id_vendor_id.to_string_lossy(),
        id_model_id.to_string_lossy(),
        id_revision.to_string_lossy()
    );
    Ok(product)
}

fn main() -> Result<()> {
    let matches = Command::new("leguichet-udev")
        .version(crate_version!())
        .author("Stephane N")
        .about("LeGuichet-udev for USB devices verification.")
        .arg(
            Arg::new("pubkey")
                .short('p')
                .long("pubkey")
                .value_name("/path/to/public.pub")
                .help("The path to public key (Default is /etc/leguichet/leguichet.pub).")
                .takes_value(true),
        )
        .arg(
            Arg::new("fstype")
                .short('f')
                .long("fstype")
                .value_name("vfat")
                .help("The filesystem type for mounting usb devices (Default is vfat).")
                .takes_value(true),
        )
        .get_matches();

    let pubkey: &str = matches
        .value_of("pubkey")
        .unwrap_or("/etc/leguichet/leguichet.pub");
    let pubkey_path = pubkey.to_string();
    let pubkey_path = Arc::new(pubkey_path);

    let server = TcpListener::bind("127.0.0.1:3013")?;
    for stream in server.incoming() {
        let pubkey_path = Arc::clone(&pubkey_path);
        spawn(move || -> Result<()> {
            let callback = |_req: &Request, response: Response| {
                println!("Leguichet-udev: Received a new websocket handshake.");
                //println!("The request's path is: {}", req.uri().path());
                //println!("The request's headers are:");
                //for (ref header, _value) in req.headers() {
                //    println!("* {}", header);
                //}

                // Let's add an additional header to our response to the client.
                //let headers = response.headers_mut();
                //headers.append("Leguichet-udev", "true".parse().unwrap());

                Ok(response)
            };
            let mut websocket = accept_hdr(stream?, callback)?;

            let mut socket = udev::MonitorBuilder::new()?
                .match_subsystem("block")?
                .listen()?;

            let mut fds = vec![pollfd {
                fd: socket.as_raw_fd(),
                events: POLLIN,
                revents: 0,
            }];
            let mut keys_in = vec![];
            let mut keys_out = vec![];
            let mut keys_undef = vec![];

            loop {
                let result = unsafe {
                    ppoll(
                        fds[..].as_mut_ptr(),
                        fds.len() as nfds_t,
                        ptr::null_mut(),
                        ptr::null(),
                    )
                };

                if result < 0 {
                    println!("ppoll error: result is < 0.");
                }

                let event = match socket.next() {
                    Some(evt) => evt,
                    None => {
                        sthread::sleep(Duration::from_millis(10));
                        continue;
                    }
                };

                //println!("Event: {:?}", event.event_type());
                if event.action() == Some(OsStr::new("add"))
                    && event.property_value(
                        OsStr::new("MINOR")
                            .to_str()
                            .ok_or_else(|| anyhow!("Cannot convert MINOR to str."))?,
                    ) == Some(OsStr::new("0"))
                {
                    let id_vendor_id = event
                        .property_value(
                            OsStr::new("ID_VENDOR_ID")
                                .to_str()
                                .ok_or_else(|| anyhow!("Cannot convert ID_VENDOR_ID to str."))?,
                        )
                        .ok_or_else(|| anyhow!("Cannot get ID_VENDOR_ID from event."))?;
                    let id_model_id = event
                        .property_value(
                            OsStr::new("ID_MODEL_ID")
                                .to_str()
                                .ok_or_else(|| anyhow!("Cannot convert ID_MODEL_ID to str."))?,
                        )
                        .ok_or_else(|| anyhow!("Cannot get ID_MODEL_ID from event."))?;
                    let id_revision = event
                        .property_value(
                            OsStr::new("ID_REVISION")
                                .to_str()
                                .ok_or_else(|| anyhow!("Cannot convert ID_REVISION to str."))?,
                        )
                        .ok_or_else(|| anyhow!("Cannot get ID_REVISION from event."))?;
                    let device = event
                        .property_value(
                            OsStr::new("DEVNAME")
                                .to_str()
                                .ok_or_else(|| anyhow!("Cannot get DEVNAME from event."))?,
                        )
                        .ok_or_else(|| anyhow!("Cannot get DEVNAME from event."))?;
                    let id_serial = event
                        .property_value(
                            OsStr::new("ID_SERIAL")
                                .to_str()
                                .ok_or_else(|| anyhow!("Cannot convert ID_SERIAL to str."))?,
                        )
                        .ok_or_else(|| anyhow!("Cannot get ID_SERIAL from event."))?;
                    //println!("device: {:?}", event.device().parent().unwrap().property_value(OsStr::new("system_name")));
                    //for property in event.device().parent() {
                    //    for attr in property.attributes() {
                    //        println!("{:?}:{:?}", attr.name(),attr.value());
                    //        //println!("{:?} = {:?}", property.name(), property.value());
                    //}
                    //    }
                    println!("New USB device found: {}", device.to_string_lossy());
                    let product = format!(
                        "{}/{}/{}",
                        id_vendor_id.to_string_lossy(),
                        id_model_id.to_string_lossy(),
                        id_revision.to_string_lossy()
                    );

                    let id_vendor_id = id_vendor_id
                        .to_str()
                        .ok_or_else(|| anyhow!("Cannot convert id_vendor_id to str."))?;
                    let id_model_id = id_model_id
                        .to_str()
                        .ok_or_else(|| anyhow!("Cannot convert id_model_id to str."))?;
                    let id_revision = id_revision
                        .to_str()
                        .ok_or_else(|| anyhow!("Cannot convert id_revision to str."))?;
                    let device = device
                        .to_str()
                        .ok_or_else(|| anyhow!("Cannot convert device to str."))?;
                    let id_serial = id_serial
                        .to_str()
                        .ok_or_else(|| anyhow!("Cannot convert id_serial to str."))?;

                    let signed = is_signed(
                        device,
                        &pubkey_path,
                        id_vendor_id,
                        id_model_id,
                        id_revision,
                        id_serial,
                    );
                    match signed {
                        Ok(value) => {
                            //Invalid Signature
                            println!("Value is {}", value);
                            if !value {
                                println!("Device signature is not valid !");
                                let keys_in_iter: Vec<String> =
                                    keys_in.clone().into_iter().collect();
                                if !keys_in_iter.contains(&product) {
                                    busy_in()?;
                                    keys_in.push(product);
                                    let device = format!("{}{}", device, "1");
                                    let keys: Usbkeys = Usbkeys {
                                        usb_in: keys_in.clone(),
                                        usb_out: keys_out.clone(),
                                        usb_undef: keys_undef.clone(),
                                    };
                                    let serialized = serde_json::to_string(&keys)?;
                                    websocket.write_message(Message::Text(serialized))?;
                                    copy_device_in(Path::new(&device))?;
                                    println!("Unsigned USB device done.");
                                    ready_in()?;
                                }
                            //Signature ok
                            } else if value {
                                println!("USB device is signed...");
                                let keys_out_iter: Vec<String> =
                                    keys_out.clone().into_iter().collect();
                                if !keys_out_iter.contains(&product) {
                                    busy_out()?;
                                    keys_out.push(product);
                                    let device = format!("{}{}", device, "1");
                                    let keys: Usbkeys = Usbkeys {
                                        usb_in: keys_in.clone(),
                                        usb_out: keys_out.clone(),
                                        usb_undef: keys_undef.clone(),
                                    };
                                    let serialized = serde_json::to_string(&keys)?;
                                    websocket.write_message(Message::Text(serialized))?;
                                    move_device_out(Path::new(&device))?;
                                    println!("Signed USB device done.");
                                    ready_out()?;
                                }
                            } else {
                                let keys_undef_iter: Vec<String> =
                                    keys_undef.clone().into_iter().collect();
                                if !keys_undef_iter.contains(&product) {
                                    keys_undef.push(product);
                                    println!("Undefined USB device.");
                                    let keys: Usbkeys = Usbkeys {
                                        usb_in: keys_in.clone(),
                                        usb_out: keys_out.clone(),
                                        usb_undef: keys_undef.clone(),
                                    };
                                    let serialized = serde_json::to_string(&keys)?;
                                    websocket.write_message(Message::Text(serialized))?;
                                }
                            }
                        }
                        Err(e) => {
                            println!("USB device never signed: {}", e);
                            let keys_in_iter: Vec<String> = keys_in.clone().into_iter().collect();
                            if !keys_in_iter.contains(&product) {
                                busy_out()?;
                                keys_in.push(product);
                                let keys: Usbkeys = Usbkeys {
                                    usb_in: keys_in.clone(),
                                    usb_out: keys_out.clone(),
                                    usb_undef: keys_undef.clone(),
                                };
                                let serialized = serde_json::to_string(&keys)?;
                                websocket.write_message(Message::Text(serialized))?;
                                let device = format!("{}{}", device, "1");
                                copy_device_in(Path::new(&device))?;
                                //copy_files_in(&mount_point)?;
                                //umount_device(&mount_point)?;
                                println!("Unsigned USB device done.");
                                ready_out()?;
                                //drop(mount_point);
                            }
                        }
                    };
                } else if event.action() == Some(OsStr::new("remove")) {
                    let product = match get_attr_udev(event) {
                        Ok(product) => product,
                        Err(_) => String::from("unknown"),
                    };

                    if product.contains("unknown") {
                        keys_in.clear();
                        keys_out.clear();
                    } else {
                        keys_out.retain(|x| *x != product);
                        keys_in.retain(|x| *x != product);
                        keys_undef.retain(|x| *x != product);
                    }
                    let keys: Usbkeys = Usbkeys {
                        usb_in: keys_in.clone(),
                        usb_out: keys_out.clone(),
                        usb_undef: keys_undef.clone(),
                    };

                    let serialized = serde_json::to_string(&keys)?;
                    websocket.write_message(Message::Text(serialized))?;
                }

                sthread::sleep(Duration::from_millis(100));
            }
        });
    }
    Ok(())
}
