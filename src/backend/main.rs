use std::{net::TcpListener, path::Path, thread::spawn};

use anyhow::Result;
use regex::Regex;
use std::fs;
use std::path::PathBuf;
use std::process::Command;
use std::thread;
use std::time::Duration;
use tungstenite::{
    accept_hdr,
    handshake::server::{Request, Response},
    Message,
};

extern crate serde;
extern crate serde_json;
#[macro_use]
extern crate serde_derive;

extern crate libc;
extern crate regex;
mod errors;

#[derive(Serialize, Deserialize, Debug)]
pub struct GlobalStatus {
    health: Daemons,
    guichetin: GuichetState,
    guichettransit: bool,
    guichetout: GuichetState,
    has_signed_once: bool,
    keypair_generated: bool,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Daemons {
    pub status_in: bool,
    pub status_transit: bool,
    pub status_out: bool,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct GuichetState {
    pub name: String,
    //pub detected: bool,
    //pub waiting: bool,
    //pub writing: bool,
    pub analysing: bool,
    //pub reading: bool,
    //pub done: bool,
    pub files: Vec<String>,
}

/// List files in a directory except hidden ones
pub fn list_files(directory: &str) -> Result<Vec<String>> {
    let paths: std::fs::ReadDir = fs::read_dir(directory)?;
    let mut names = paths
        .filter_map(|entry| {
            entry.ok().and_then(|e| {
                e.path()
                    .file_name()
                    .and_then(|n| n.to_str().map(String::from))
            })
        })
        .collect::<Vec<String>>();
    // Not sending any files starting with dot like .bashrc
    let re = Regex::new(r"^\.([a-z])*")?;
    names.retain(|x| !re.is_match(x));
    Ok(names)
}

pub fn daemon_status() -> Result<[bool; 3]> {
    let mut state: [bool; 3] = [true, true, true];

    let output = Command::new("systemctl")
        .arg("status")
        .arg("leguichet-in.service")
        .output()
        .expect("failed to get status for leguichet-in");
    let status_in = String::from_utf8_lossy(&output.stdout);
    let re = Regex::new(r"Active: active")?;
    if re.is_match(&status_in) {
        state[0] = true;
    } else {
        state[0] = false;
    }
    let output = Command::new("systemctl")
        .arg("status")
        .arg("leguichet-transit.service")
        .output()
        .expect("failed to get status for leguichet-transit");
    let status_in = String::from_utf8_lossy(&output.stdout);
    let re = Regex::new(r"Active: active")?;
    if re.is_match(&status_in) {
        state[1] = true;
    } else {
        state[1] = false;
    }
    let output = Command::new("systemctl")
        .arg("status")
        .arg("leguichet-out.service")
        .output()
        .expect("failed to get status for leguichet-out");
    let status_in = String::from_utf8_lossy(&output.stdout);
    let re = Regex::new(r"Active: active")?;
    if re.is_match(&status_in) {
        state[2] = true;
    } else {
        state[2] = false;
    }
    Ok(state)
}

fn main() -> Result<()> {
    let server = TcpListener::bind("127.0.0.1:3012")?;
    for stream in server.incoming() {
        spawn(move || -> Result<()> {
            let callback = |_req: &Request, mut response: Response| {
                println!("Leguichet-backend: Received a new websocket handshake.");
                //println!("The request's path is: {}", req.uri().path());
                //println!("The request's headers are:");
                //for (ref header, _value) in req.headers() {
                //    println!("* {}", header);
                //}

                // Let's add an additional header to our response to the client.
                let headers = response.headers_mut();
                headers.append("Leguichet-backend", "true".parse().unwrap());

                Ok(response)
            };
            let mut websocket = accept_hdr(stream?, callback)?;

            loop {
                let files_in = list_files("/var/local/in");
                let files_out = list_files("/var/local/out");

                let mut diode_in = PathBuf::new();
                diode_in.push("/run/diode-in");
                let mut diode_out = PathBuf::new();
                diode_out.push("/run/diode-out/");
                let is_empty_in = diode_in.read_dir()?.next().is_none();
                let is_empty_out = diode_out.read_dir()?.next().is_none();

                let working_in =
                    !is_empty_in || Path::new("/var/lock/leguichet/leguichet-in").exists();

                let working_out =
                    !is_empty_out || Path::new("/var/lock/leguichet/leguichet-out").exists();

                let working_transit = !(is_empty_out
                    && is_empty_in
                    && !Path::new("/var/lock/leguichet/leguichet-transit").exists());

                let health: Daemons = Daemons {
                    status_in: daemon_status()?[0],
                    status_transit: daemon_status()?[1],
                    status_out: daemon_status()?[2],
                };
                let guichet_state_in: GuichetState = GuichetState {
                    name: String::from("GUICHET-IN"),
                    analysing: working_in,
                    files: files_in?,
                };
                let guichet_state_out: GuichetState = GuichetState {
                    name: String::from("GUICHET-OUT"),
                    analysing: working_out,
                    files: files_out?,
                };
                let mut has_signed = false;

                if !Path::new("/usr/share/leguichet/neversigned").exists() {
                    has_signed = true;
                }
                let mut keypair_ok = false;

                if Path::new("/etc/leguichet/leguichet.priv").exists()
                    && Path::new("/etc/leguichet/leguichet.pub").exists()
                {
                    keypair_ok = true;
                }

                let orders = GlobalStatus {
                    health,
                    guichetin: guichet_state_in,
                    guichettransit: working_transit,
                    guichetout: guichet_state_out,
                    has_signed_once: has_signed,
                    keypair_generated: keypair_ok,
                };

                let serialized = serde_json::to_string(&orders)?;
                websocket.write_message(Message::Text(serialized))?;
                thread::sleep(Duration::from_millis(100));
            }
        });
    }
    Ok(())
}
